package ch.zli.bch.examorganizer.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import ch.zli.bch.examorganizer.R;
import ch.zli.bch.examorganizer.model.Exam;
import ch.zli.bch.examorganizer.services.ExamService;

public class DetailsActivity extends AppCompatActivity {

    ExamService mService;
    boolean mBound = false;

    TextView title_TextView;
    TextView date_TextView;
    TextView learningGoals_TextView;
    ImageButton editButton;
    ImageButton deleteButton;


    String title;
    String learningGoals;
    String date;

    Exam exam;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        title_TextView = findViewById(R.id.detailsTitle_text);
        date_TextView = findViewById(R.id.detailsDate_text);
        learningGoals_TextView = findViewById(R.id.detailsLearningGoals_text);
        editButton = findViewById(R.id.details_editButton);
        deleteButton = findViewById(R.id.details_deleteButton);

        getData();
        setData();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ExamActivity.class));
            }
        });

        editButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra("exam_edit", exam);
            startActivity(intent);
        });

        deleteButton.setOnClickListener(view -> {
            deleteExam(exam.getId()-1);
            startActivity(new Intent(getApplicationContext(), ExamActivity.class));
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void deleteExam(int i) {
        if (mBound) {
            mService.deleteExam(i);
        } else {
            Toast.makeText(this, "It seems delete doesn't work :/", Toast.LENGTH_LONG).show();
        }
    }



    private void getData(){
        if(getIntent().hasExtra("exam")){
            Intent intent = getIntent();
            exam = (Exam) intent.getSerializableExtra("exam");
        } else {
            Toast.makeText(this, "No Data", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        title = exam.getTitle();
        learningGoals = exam.getLearningGoals();
        date = exam.getDate();

        title_TextView.setText(title);
        learningGoals_TextView.setText(learningGoals);
        date_TextView.setText(date);

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ExamService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ExamService.LocalBinder binder = (ExamService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}