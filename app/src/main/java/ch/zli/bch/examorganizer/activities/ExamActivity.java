package ch.zli.bch.examorganizer.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


import ch.zli.bch.examorganizer.R;
import ch.zli.bch.examorganizer.model.Exam;
import ch.zli.bch.examorganizer.recyclerView.RecyclerAdapter;
import ch.zli.bch.examorganizer.services.ExamService;

public class ExamActivity extends AppCompatActivity {

    ExamService mService;
    boolean mBound = false;

    ArrayList<Exam> exams = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);

        //Variables
        FloatingActionButton floatingActionButton = findViewById(R.id.fab_createExam);

        //Actions
        floatingActionButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, CreateActivity.class);
            startActivity(intent);
        });
        recyclerView = findViewById(R.id.recycler_view);
        recyclerAdapter = new RecyclerAdapter(ExamActivity.this, exams);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(ExamActivity.this));


    }

    public ArrayList<Exam> getExams() {
        if (mBound) {
            return mService.getExams();
        } else {
            Toast.makeText(this, "It seems there is no Array :/", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public void loadExams() {
        if (mBound) {
            mService.loadExams();
        } else {
            Toast.makeText(this, "It seems loading doesn't work :/", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ExamService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ExamService.LocalBinder binder = (ExamService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            loadExams();
            //update RecyclerView
            exams.clear();
            exams.addAll(getExams());
            recyclerAdapter.notifyDataSetChanged();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}