package ch.zli.bch.examorganizer.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

import ch.zli.bch.examorganizer.R;
import ch.zli.bch.examorganizer.services.ExamService;

public class CreateActivity extends AppCompatActivity {

    ExamService mService;
    boolean mBound = false;
    String date;

    EditText examName_text;
    EditText examLearningGoals_text;
    EditText examDate_text;
    ImageButton calendarButton;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        examName_text = findViewById(R.id.examName_textInput);
        examLearningGoals_text = findViewById(R.id.examLearningGoals_textInput);
        examDate_text = findViewById(R.id.examDate_textInput);

        calendarButton = findViewById(R.id.calendarButton);
        saveButton = findViewById(R.id.saveButton);

        examDate_text.setOnClickListener(view -> {
            createDatePicker();
        });
        calendarButton.setOnClickListener(view -> {
            createDatePicker();
        });

        saveButton.setOnClickListener(view -> {
            String name = examName_text.getText().toString();
            String learningGoals = examLearningGoals_text.getText().toString();

            createExam(name, learningGoals, date);
            startActivity(new Intent(getApplicationContext(), ExamActivity.class));
        });

    }

    public void createDatePicker() {
        Calendar startValue = Calendar.getInstance();
        int year = startValue.get(Calendar.YEAR);
        int month = startValue.get(Calendar.MONTH);
        int day = startValue.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                // Formatierung um bei Zahlen im Datum unter 10 eine "0" vorne hinzuzufügen
                // 3-3-2021 wird zu -> 03-03-2021
                if ((month + 1) < 10 && dayOfMonth < 10) {
                    examDate_text.setText("0" + dayOfMonth + "-0" + (month + 1) + "-" + year);
                    // da der Datepicker 0-11 rechnet in den Monaten, wird hier + 1 addiert um 1-12 zu verwenden
                    date = "0" + dayOfMonth + "-0" + (month + 1) + "-" + year + "";
                } else if (dayOfMonth < 10) {
                    examDate_text.setText("0" + dayOfMonth + "-" + (month + 1) + "-" + year);
                    date = "0" + dayOfMonth + "-" + (month + 1) + "-" + year + "";
                } else if ((month + 1) < 10) {
                    examDate_text.setText("" + dayOfMonth + "-0" + (month + 1) + "-" + year);
                    date = "" + dayOfMonth + "-0" + (month + 1) + "-" + year + "";
                } else {
                    examDate_text.setText("" + dayOfMonth + "-" + (month + 1) + "-" + year);
                    date = "" + year + "-" + (month + 1) + "-" + dayOfMonth + "";

                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public void createExam(String title, String learningGoals, String date) {
        if (mBound) {
            mService.createExam(title, learningGoals, date);
        } else {
            Toast.makeText(this, "It seems save doesn't work :/", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ExamService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ExamService.LocalBinder binder = (ExamService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}