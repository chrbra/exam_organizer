package ch.zli.bch.examorganizer.model;

import java.io.Serializable;

public class Exam implements Serializable {
    int id;
    String title;
    String learningGoals;
    String date;


    public Exam() {
    }

    public Exam(int id, String title, String learningGoals, String date) {
        this.id = id;
        this.title = title;
        this.learningGoals = learningGoals;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLearningGoals() {
        return learningGoals;
    }

    public void setLearningGoals(String learningGoals) {
        this.learningGoals = learningGoals;
    }
}
