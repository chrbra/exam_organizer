package ch.zli.bch.examorganizer.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Calendar;

import ch.zli.bch.examorganizer.R;
import ch.zli.bch.examorganizer.model.Exam;
import ch.zli.bch.examorganizer.services.ExamService;

public class EditActivity extends AppCompatActivity {

    ExamService mService;
    boolean mBound = false;

    Exam exam;
    String title;
    String learningGoals;
    String date;


    EditText examName_EditText;
    EditText examLearningGoals_EditText;
    EditText examDate_EditText;
    ImageButton calendarButton;
    Button saveButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        examName_EditText = findViewById(R.id.edit_ExamName);
        examLearningGoals_EditText = findViewById(R.id.edit_ExamLearningGoals);
        examDate_EditText = findViewById(R.id.edit_ExamDate);
        calendarButton = findViewById(R.id.edit_calendarButton);
        saveButton = findViewById(R.id.edit_saveButton);
        getData();
        setData();

        calendarButton.setOnClickListener(view -> {
            createDatePicker();
        });

        saveButton.setOnClickListener(view -> {
            String name = examName_EditText.getText().toString();
            String learningGoals = examLearningGoals_EditText.getText().toString();

            Exam updated_exam = new Exam(exam.getId(), name, learningGoals, date);

            editExam(updated_exam);
            startActivity(new Intent(getApplicationContext(), ExamActivity.class));
        });
    }

    public void createDatePicker(){
        Calendar startValue = Calendar.getInstance();
        int year=startValue.get(Calendar.YEAR);
        int month=startValue.get(Calendar.MONTH);
        int day=startValue.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                // Formatierung um bei Zahlen im Datum unter 10 eine "0" vorne hinzuzufügen
                // 3-3-2021 wird zu -> 03-03-2021
                if((month+1) <10 && dayOfMonth < 10) {
                    examDate_EditText.setText("0" + dayOfMonth + "-0" + (month+1) + "-" + year);
                    // da der Datepicker 0-11 rechnet in den Monaten, wird hier + 1 addiert um 1-12 zu verwenden
                    date = "0" + dayOfMonth + "-0" + (month + 1) + "-" + year + "";
                } else if(dayOfMonth <10){
                    examDate_EditText.setText("0" + dayOfMonth + "-" + (month+1) + "-" + year);
                    date = "0" + dayOfMonth + "-" + (month + 1) + "-" + year + "";
                } else if((month+1) <10) {
                    examDate_EditText.setText("" + dayOfMonth + "-0" + (month+1) + "-" + year);
                    date = ""+dayOfMonth+"-0"+(month+1)+"-"+year+"";
                } else {
                    examDate_EditText.setText("" + dayOfMonth + "-" + (month+1) + "-" + year);
                    date = ""+year+"-"+(month+1)+"-"+dayOfMonth+"";

                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void getData(){
        if(getIntent().hasExtra("exam_edit")){
            Intent intent = getIntent();
            exam = (Exam) intent.getSerializableExtra("exam_edit");
        } else {
            Toast.makeText(this, "No Data", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        title = exam.getTitle();
        learningGoals = exam.getLearningGoals();
        date = exam.getDate();

        examName_EditText.setText(title);
        examLearningGoals_EditText.setText(learningGoals);
        examDate_EditText.setText(date);

    }

    public void editExam(Exam exam) {
        if (mBound) {
            mService.updateExam(exam);
        } else {
            Toast.makeText(this, "It seems delete doesn't work :/", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ExamService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ExamService.LocalBinder binder = (ExamService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}