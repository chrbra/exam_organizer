package ch.zli.bch.examorganizer.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ch.zli.bch.examorganizer.R;
import ch.zli.bch.examorganizer.activities.DetailsActivity;
import ch.zli.bch.examorganizer.model.Exam;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    ArrayList<Exam> exams;
    Context context;

    public RecyclerAdapter(Context context, ArrayList<Exam> exams){
        this.context = context;
        this.exams = exams;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //setze einen Titel und das Datum für das Item
        holder.title_textView.setText(exams.get(position).getTitle());
        holder.date_textView.setText(exams.get(position).getDate());

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailsActivity.class);
                //übergebe das Exam Objekt beim Click auf das bestimmte Item
                intent.putExtra("exam", exams.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return exams.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView title_textView;
        TextView date_textView;
        TextView learningGoals_textView;
        ConstraintLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title_textView = itemView.findViewById(R.id.title_text_view);
            date_textView = itemView.findViewById(R.id.date_text_view);
            learningGoals_textView = itemView.findViewById(R.id.detailsLearningGoals_text);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }
}
