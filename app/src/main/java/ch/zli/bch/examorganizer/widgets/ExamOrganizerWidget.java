package ch.zli.bch.examorganizer.widgets;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;

import ch.zli.bch.examorganizer.R;
import ch.zli.bch.examorganizer.model.Exam;
import ch.zli.bch.examorganizer.services.ExamService;

/**
 * Implementation of App Widget functionality.
 */
public class ExamOrganizerWidget extends AppWidgetProvider {

    RemoteViews views;

    ExamService mService;
    boolean mBound = false;
    AppWidgetManager appWidgetManager;
    int appWidgetId;


    Exam exam;
    String title;
    String date;
    String learningGoals;

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        this.appWidgetManager = appWidgetManager;

        CharSequence widgetText = context.getString(R.string.appwidget_title);
        CharSequence widgetText2 = context.getString(R.string.appwidget_date);

        //starts the connection between ExamService & Widget
        Intent intent = new Intent(context, ExamService.class);
        context.startService(intent);
        context.getApplicationContext().bindService(intent, connection, Context.BIND_AUTO_CREATE);

        // Construct the RemoteViews object
        views = new RemoteViews(context.getPackageName(), R.layout.exam_organizer_widget);

        views.setTextViewText(R.id.appwidget_text, widgetText);
        views.setTextViewText(R.id.appwidget_text2, widgetText2);

//         Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public void setLatestExam() throws ParseException {
        if(mBound) {
            exam = mService.findLatestExam();
            System.out.println(exam.getTitle());

            views.setTextViewText(R.id.appwidget_text, exam.getTitle());
            try{
                appWidgetManager.updateAppWidget(appWidgetId, views);
            } catch (Exception e){
                System.out.println(e);
            }

        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ExamService.LocalBinder binder = (ExamService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
//            try {
//                setLatestExam();
//            } catch (ParseException e) {
//                System.out.println("Mission Failed");
//            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}