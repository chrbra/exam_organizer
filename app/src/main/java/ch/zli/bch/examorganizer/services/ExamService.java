package ch.zli.bch.examorganizer.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ch.zli.bch.examorganizer.model.Exam;

public class ExamService extends Service {

    // Binder given to clients
    private final IBinder binder = new ExamService.LocalBinder();

    ArrayList<Exam> exams = new ArrayList<>();

    public ArrayList<Exam> getExams() {
        return exams;
    }

    public class LocalBinder extends Binder {
        public ExamService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ExamService.this;
        }
    }

    public ExamService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * method for clients
     */
    public void createExam(String title, String learningGoals, String date) {
        final SharedPreferences sharedPreferences = getSharedPreferences("Exams", MODE_PRIVATE);
        Exam exam = new Exam(exams.size()+1, title, learningGoals, date);
        //füge die Prüfung zu dem bestehenden Prüfungarray hinzu
        exams.add(exam);
        Gson gson = new Gson();
        //verwandelt das Array "exams" in einen JSON String
        String json = gson.toJson(exams);
        System.out.println("EXAMMMM!!!!!"+json);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Exams", json);
        editor.apply();
    }



    public void loadExams() {
        final SharedPreferences sharedPreferences = getSharedPreferences("Exams", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Exams", "");
        if (json.isEmpty()) {
            Toast.makeText(this, "No Exams available", Toast.LENGTH_LONG).show();
        } else {
            //bestimmt den Datentyp in was der JSON String mithilfe von GJSON umgewandelt werden soll.
            Type type = new TypeToken<List<Exam>>() {}.getType();
            exams = gson.fromJson(json, type);
        }
    }

    public void updateExam(Exam exam){
        final SharedPreferences sharedPreferences = getSharedPreferences("Exams", MODE_PRIVATE);
        exams.set(exam.getId()-1, exam);
        Gson gson = new Gson();
        //verwandelt das Array "exams" in einen JSON String
        String json = gson.toJson(exams);
        System.out.println("EXAMMMM!!!!!"+json);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Exams", json);
        editor.apply();
    }

    public void deleteExam(int i){
        final SharedPreferences sharedPreferences = getSharedPreferences("Exams", MODE_PRIVATE);
        exams.remove(i);
        Gson gson = new Gson();
        //verwandelt das Array "exams" in einen JSON String
        String json = gson.toJson(exams);
        System.out.println("EXAMMMM!!!!!"+json);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Exams", json);
        editor.apply();
    }

    public Exam findLatestExam() throws ParseException {

        @SuppressLint("SimpleDateFormat")
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = exams.get(exams.size()-1).getDate();
        Date latestDate = format.parse(dateString);
        int examId = exams.size()-1;

        //iterate through exams and find the closest exam from today
        for(Exam exam : exams){
            Date date = format.parse(exam.getDate());
            if(date!=null){
                if(date.before(latestDate)){
                    latestDate = date;
                    examId = exam.getId()-1;
                }
            }
        }
        return exams.get(examId);
    }

}